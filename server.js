const http = require('http')
const https = require('https')
const express = require('express')
const cors = require('cors')
const path = require('path')
const fs = require('fs')
const { createProxyMiddleware } = require('http-proxy-middleware')

const app = express()
app.enable('trust proxy')

app.use(function (req, res, next) {
  if (req.secure) {
    next()
  } else {
    res.redirect('https://' + req.headers.host + req.url);
  }
})

app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/api/v2', createProxyMiddleware({
  target: 'http://103.232.123.183:1337',
  changeOrigin: true,
  prependPath: false,
  pathRewrite: {
    '^/api/v2/bdmt-news': '/bdmt-news',
    '^/api/v2/bdmt-products': '/products',
    '^/api/v2/bdmt-users': '/bdmt-users',
    '^/api/v2/uploads': '/uploads',
  }
}))


app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'home.html')))

app.use((err, req, res, _next) => res.status(500).json({ message: err.message || String(err) }))

const privateKey = fs.readFileSync(path.join(__dirname, 'sslcert', 'server.key'), 'utf8')
const certificate = fs.readFileSync(path.join(__dirname, 'sslcert', 'server.crt'), 'utf8')
const credentials = { key: privateKey, cert: certificate }

const httpServer = http.createServer(app)
const httpsServer = https.createServer(credentials, app)

httpServer.listen(80, (err) => {
  if (err) {
    console.error(`Start server error`, err)
  } else {
    console.log(`Server is listening on port 80`)
  }
})

httpsServer.listen(443, (err) => {
  if (err) {
    console.error(`Start server error`, err)
  } else {
    console.log(`Server is listening on port 443`)
  }
})
