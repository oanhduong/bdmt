# Use an official Node runtime as a parent image
FROM node:12.7.0-alpine

RUN mkdir -p /usr/src/app

COPY . /app/server
WORKDIR /app/server
RUN npm install

# COPY client /app/client
# WORKDIR /app/client
# RUN npm install
# RUN npm run build

# WORKDIR /app
# RUN cp -R ./server/* .
# RUN cp -R ./client/build/* ./public
# RUN rm -rf ./server
# RUN rm -rf ./client

ENV PORT=80

EXPOSE $PORT
CMD ["node", "server.js"]

