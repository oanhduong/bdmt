const addDetail = ({ title, subTitle, newPrice, oldPrice, image, status,
        preserve, packing, howToUse, richContent, expiredDate, unit }) => {
    $(`#details`).prepend(
        `
        <h3 class="product-title">${title}</h3>
        <p class="product-description">${subTitle}</p>
        <h4 class="price"><span class="current-price">${format(newPrice, unit)}</span> <span
                class="old-price">${format(oldPrice, unit)}</span></h4>
        <h6>
            Tình trạng: <span class="status detail-style">${status}</span>
        </h6>
        <h6>
            Cách bảo quản: <span class="preservation detail-style">${preserve}</span>
        </h6>
        <h6>
            Cách dùng: <span class="using detail-style">${howToUse}</span>
        </h6>
        <h6>
            Bao bì đóng gói: <span class="using detail-style">${packing}</span>
        </h6>
        <h6>
            Hạn sử dụng: <span class="using detail-style">${expiredDate}</span>
        </h6>
        `
    )

    var converter = new showdown.Converter()
    var html = `<div>${converter.makeHtml(richContent)}</div>`
    var nodes = $(html)
    nodes.find('img').each(function () {
        const oldSrc = $(this).attr('src')
        const newSrc = oldSrc.replace('/uploads', '/api/v2/uploads')
        $(this).attr('src', newSrc)
    })
    finalHtml = nodes.html()
    $(`#desc`).append(finalHtml)

    const images = image.map((i) => `api/v2${i.url}`)
    images.forEach((i, idx) => {
        const id = `pic-${idx + 1}`
        const target = `#pic-${idx + 1}`

        $('#previewTab').append(`
        <div class="tab-pane ${idx == 0 ?  'active' : ''}" id="${id}"><img class="pop"
            src="${i}" />
        </div>
        `)
        
        $('#previewThumb').append(`
        <li ${idx == 0 ?  'class="active"' : ''}><a data-target="${target}" data-toggle="tab"><img
            src="${i}" /></a>
        </li>
        `)
    })
}

var getParam = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

const format = (price, unit) => price ? price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}) + unit : ''

const addRelatedProduct = ({ title, newPrice, oldPrice, image, seolink, unit }) => {
    $('#related').append(
        `
        <div id="${seolink}" class="col-md-4 col-sm-6">
            <div class="pro-bg-style">
                <a href="product-detail.html">
                <div class="img-pro">
                    <img src="${image}" alt="">
                </div>
                <h4 class="pro-name">${title}</h4>
                <h3 class="pro-price">${format(newPrice, unit)}<span class="old-price">${format(oldPrice, unit)}</span></h3>
            </a>
            </div>
        </div>
        `
    )

    $(`#${seolink}`).on('click', function () {
        window.location.href = `product-detail.html?link=${seolink}`
        return false
    })
}

const relatedProducts = (id, type) => {
    $.ajax({
        url: `/api/v2/bdmt-products?type=${type}`,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (products) {
            products.filter((e) => e && e._id != id)
                .map((e) => {
                    return {
                        title: e.title || '',
                        newPrice: e.newPrice || '',
                        oldPrice: e.oldPrice || '',
                        seolink: e.seolink || '',
                        unit: e.unit ? `/${e.unit}` : '',
                        image: e.image[0] && e.image[0].url ? `/api/v2${e.image[0].url}` : 'assets/img/nemchuanon-1024x1024.jpg',
                    }
                })
                .forEach((e) => {
                    addRelatedProduct(e)
                })
        },
    });
}

const addNews = ({ title, seolink }) => {
    $('#news').append(
        `
        <div class="col-12 news-more-style">
            <div class="d-flex align-items-center pb-9">
                <!--begin::Section-->
                <div class="d-flex flex-column flex-grow-1">
                    <!--begin::Title-->
                    <a href="news-detail.html?link=${seolink}"
                        class=" font-weight-bolder text-hover-primary font-size-lg mb-1">${title}</a>
                    <!--end::Title-->

                </div>
                <!--end::Section-->
            </div>
        </div>
        `
    )
}

const news = () => {
    $.ajax({
        url: '/api/v2/bdmt-news',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (news) {
            news.filter((e) => e)
                .map((e) => {
                    return {
                        title: e.title || '',
                        seolink: e.seolink
                    }
                })
                .forEach((e) => addNews(e))
        },
    });
}

$(document).ready(function ($) {
    $.ajax({
        url: `/api/v2/bdmt-products?seolink=${getParam('link')}`,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (products) {
            products.filter((e) => e)
                .map((e) => {
                    return {
                        title: e.title || '',
                        subTitle: e.subTitle || '',
                        status: e.status || '',
                        preserve: e.preserve || '',
                        packing: e.packing || '',
                        howToUse: e.howToUse || '',
                        newPrice: e.newPrice || '',
                        oldPrice: e.oldPrice || '',
                        type: e.type || '',
                        richContent: e.richContent || '',
                        image: e.image || [],
                        id: e._id || '',
                        expiredDate: e.expiredDate || '',
                        unit: e.unit ? `/${e.unit}` : ''
                    }
                })
                .forEach((e) => {
                    addDetail(e)
                    relatedProducts(e.id, e.type)
                })
        },
    });
    news()
})

