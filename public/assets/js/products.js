const addNew = ({ title, newPrice, oldPrice, image, type, seolink, unit }) => {
    $(`#${type}`).append(
        `
        <div id="${seolink}" class="col-md-4 col-sm-6">
            <div class="pro-bg-style">
                <a href="product-detail.html">
                <div class="img-pro">
                    <img src="${image}" alt="">
                </div>
                <h4 class="pro-name">${title}</h4>
                <h3 class="pro-price">${format(newPrice, unit)}<span class="old-price">${format(oldPrice, unit)}</span></h3>
            </a>
            </div>
        </div>
        `
    )

    $(`#${seolink}`).on('click', function () {
        window.location.href = `product-detail.html?link=${seolink}`
        return false
    })
}

const format = (price, unit) => price ? price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}) + unit : ''

$(document).ready(function ($) {
    $.ajax({
        url: `/api/v2/bdmt-products`,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (products) {
            products.filter((e) => e)
                .map((e) => {
                    return {
                        title: e.title || '',
                        newPrice: e.newPrice || '',
                        oldPrice: e.oldPrice || '',
                        type: e.type || '',
                        seolink: e.seolink || '',
                        unit: e.unit ? `/${e.unit}` : '',
                        image: e.image[0] && e.image[0].url ? `api/v2${e.image[0].url}` : 'assets/img/nemchuanon-1024x1024.jpg',
                    }
                })
                .forEach((e) => addNew(e))
        },
    });
    // addNew({title: 'Tai sao tai sao', richContent: '<p>aaaa</p>', author: 'DUONG THANH OANH'})
})

