const addNew = ({ title, price, image }) => {
    $('#listProduct').append(
        `
        <div class="col-md-4 col-sm-6">
            <div class="pro-bg-style">
                <div class="img-pro">
                    <img src="${image}" alt="">
                </div>
                <h4 class="pro-name">${title}</h4>
                <h3 class="pro-price">${format(price)}</h3>
            </div>
        </div>
        `
    )
}

const format = (price) => price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})

$(document).ready(function ($) {
    $.ajax({
        url: `/api/v2/bdmt-products`,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (products) {
            products.filter((e) => e)
                .map((e) => {
                    return {
                        title: e.title || '',
                        price: e.price || '',
                        image: e.image && e.image.url ? `api/v2${e.image.url}` : 'assets/img/nemchuanon-1024x1024.jpg',
                    }
                })
                .forEach((e) => addNew(e))
        },
    });
    // addNew({title: 'Tai sao tai sao', richContent: '<p>aaaa</p>', author: 'DUONG THANH OANH'})
})

